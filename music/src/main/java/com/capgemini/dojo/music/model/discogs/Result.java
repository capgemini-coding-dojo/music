
package com.capgemini.dojo.music.model.discogs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "user_data",
    "community",
    "catno",
    "year",
    "id",
    "style",
    "thumb",
    "title",
    "label",
    "master_id",
    "type",
    "format",
    "barcode",
    "master_url",
    "genre",
    "country",
    "uri",
    "cover_image",
    "resource_url"
})
public class Result {

    @JsonProperty("user_data")
    private UserData userData;
    @JsonProperty("community")
    private Community community;
    @JsonProperty("catno")
    private String catno;
    @JsonProperty("year")
    private String year;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("style")
    private List<String> style = null;
    @JsonProperty("thumb")
    private String thumb;
    @JsonProperty("title")
    private String title;
    @JsonProperty("label")
    private List<String> label = null;
    @JsonProperty("master_id")
    private Integer masterId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("format")
    private List<String> format = null;
    @JsonProperty("barcode")
    private List<String> barcode = null;
    @JsonProperty("master_url")
    private String masterUrl;
    @JsonProperty("genre")
    private List<String> genre = null;
    @JsonProperty("country")
    private String country;
    @JsonProperty("uri")
    private String uri;
    @JsonProperty("cover_image")
    private String coverImage;
    @JsonProperty("resource_url")
    private String resourceUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("user_data")
    public UserData getUserData() {
        return userData;
    }

    @JsonProperty("user_data")
    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    @JsonProperty("community")
    public Community getCommunity() {
        return community;
    }

    @JsonProperty("community")
    public void setCommunity(Community community) {
        this.community = community;
    }

    @JsonProperty("catno")
    public String getCatno() {
        return catno;
    }

    @JsonProperty("catno")
    public void setCatno(String catno) {
        this.catno = catno;
    }

    @JsonProperty("year")
    public String getYear() {
        return year;
    }

    @JsonProperty("year")
    public void setYear(String year) {
        this.year = year;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("style")
    public List<String> getStyle() {
        return style;
    }

    @JsonProperty("style")
    public void setStyle(List<String> style) {
        this.style = style;
    }

    @JsonProperty("thumb")
    public String getThumb() {
        return thumb;
    }

    @JsonProperty("thumb")
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("label")
    public List<String> getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(List<String> label) {
        this.label = label;
    }

    @JsonProperty("master_id")
    public Integer getMasterId() {
        return masterId;
    }

    @JsonProperty("master_id")
    public void setMasterId(Integer masterId) {
        this.masterId = masterId;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("format")
    public List<String> getFormat() {
        return format;
    }

    @JsonProperty("format")
    public void setFormat(List<String> format) {
        this.format = format;
    }

    @JsonProperty("barcode")
    public List<String> getBarcode() {
        return barcode;
    }

    @JsonProperty("barcode")
    public void setBarcode(List<String> barcode) {
        this.barcode = barcode;
    }

    @JsonProperty("master_url")
    public String getMasterUrl() {
        return masterUrl;
    }

    @JsonProperty("master_url")
    public void setMasterUrl(String masterUrl) {
        this.masterUrl = masterUrl;
    }

    @JsonProperty("genre")
    public List<String> getGenre() {
        return genre;
    }

    @JsonProperty("genre")
    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("uri")
    public String getUri() {
        return uri;
    }

    @JsonProperty("uri")
    public void setUri(String uri) {
        this.uri = uri;
    }

    @JsonProperty("cover_image")
    public String getCoverImage() {
        return coverImage;
    }

    @JsonProperty("cover_image")
    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    @JsonProperty("resource_url")
    public String getResourceUrl() {
        return resourceUrl;
    }

    @JsonProperty("resource_url")
    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
