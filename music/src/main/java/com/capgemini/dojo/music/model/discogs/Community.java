
package com.capgemini.dojo.music.model.discogs;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "have",
    "want"
})
public class Community {

    @JsonProperty("have")
    private Integer have;
    @JsonProperty("want")
    private Integer want;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("have")
    public Integer getHave() {
        return have;
    }

    @JsonProperty("have")
    public void setHave(Integer have) {
        this.have = have;
    }

    @JsonProperty("want")
    public Integer getWant() {
        return want;
    }

    @JsonProperty("want")
    public void setWant(Integer want) {
        this.want = want;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
