
package com.capgemini.dojo.music.model.discogs;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "in_collection",
    "in_wantlist"
})
public class UserData {

    @JsonProperty("in_collection")
    private Boolean inCollection;
    @JsonProperty("in_wantlist")
    private Boolean inWantlist;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("in_collection")
    public Boolean getInCollection() {
        return inCollection;
    }

    @JsonProperty("in_collection")
    public void setInCollection(Boolean inCollection) {
        this.inCollection = inCollection;
    }

    @JsonProperty("in_wantlist")
    public Boolean getInWantlist() {
        return inWantlist;
    }

    @JsonProperty("in_wantlist")
    public void setInWantlist(Boolean inWantlist) {
        this.inWantlist = inWantlist;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
